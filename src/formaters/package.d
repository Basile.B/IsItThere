module formaters;

import
    std.stdio : File, stdout;
public import
    formaters.agnostic, formaters.dlang, formaters.pascal;

/**
 * The base class for writing the results.
 *
 * The methods are called following the declarations order.
 */
class Formater
{

private:

    File _file;

public:

    ///
    this(){}

    /**
     * Constructs a Formater instance.
     *
     * Params:
     *      filename = The file where the results will be formated. When an
     *          empty string is passed the results are w ritten to stdout.
     */
    final this(const string filename)
    {
        if (!filename.length)
            _file = stdout;
        else
            _file = File(filename, "w+");
    }

    final ~this()
    {
        if (_file != stdout)
            _file.close;
    }

    /**
     * Returns a reference, either to a physical file or to stdout.
     */
    final ref File file()
    {
        return _file;
    }

    /**
     * Begins formating.
     *
     * Params:
     *      name = The name of the aggregate that will be written.
     */
    abstract void begins(string name);

    /**
     * Writes the information about this dictionary.
     *
     * Params:
     *      renderInformation = date, seed, map length.
     */
    abstract void writeRenderInformation(string renderInformation);

    /**
     * Writes the dictionary.
     *
     * Params:
     *      dictionary = The original dictionary with the entries redistributed
     *          according to their hash.
     *      size = The size of the dictionary.
     */
    abstract void writeDictionary(const string[] dictionary, size_t size);

    /**
     * Writes the status of the buckets.
     *
     * Params:
     *      occupiedBuckets = An array of bool that indicates if a bucket maps
     *          to a word of the original dictionary.
     *      size = The size of the dictionary.
     */
    abstract void writeOccupiedBuckets(const bool[] occupiedBuckets, size_t size);

    /**
     * Writes the coefficients.
     *
     * Params:
     *      coefficients = The coefficients used to translate each byte in the
     *          hash function.
     *      size = The size of the dictionary.
     */
    abstract void writeCoefficients(const ref ubyte[256] coefficients);

    /**
     * Writes the hash and the test functions.
     *
     * The hashing function takes each byte of the input, translates it using the
     * coefficients and add this value to the previous result.
     *
     * Params:
     *      size = The size of the dictionary. Used as modulo RHS after the hash.
     *
     */
    abstract void writeTestFunction(size_t size, bool caseCare);

    /**
     * Ends formating.
     */
    abstract void ends();
}

